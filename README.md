### [ubinull](https://gitlab.com/ubinull) / [discord-ip-logger](https://gitlab.com/ubinull/discord-ip-logger)


Usage and setup guides are available at [discord-ip-logger wiki](https://gitlab.com/ubinull/discord-ip-logger/-/wikis/Setup).

---

Important stuff:
This program is not intended for malicius purposes.  
This program should be used on personal servers to know what its IP is.  
ubinull (ubionexd or UbiOne) does not hold any responsibility for any damage by any means caused with and by this software.
