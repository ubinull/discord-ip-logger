// Main script to send the ecnrypted string to Discord as a text file
// Use: See setup instructions, then run 'pnpm dev'
import { WebhookClient, EmbedBuilder } from "discord.js";
import { publicEncrypt } from "crypto";
import { writeFile } from "fs";
import { AttachmentBuilder } from "discord.js";
import { publicIpv4 } from "public-ip";
import * as dotenv from "dotenv";
dotenv.config();

// Get webhook (Webhook ID and Token required in .env)
const webhookClient = new WebhookClient({
  id: process.env.WEBHOOK_ID,
  token: process.env.WEBHOOK_TOKEN,
});

// Get IP
const ip = await publicIpv4();

// Encrypt (an RSA public key required in .env)
const encrypt = publicEncrypt(process.env.PUBLIC_KEY, Buffer.from(ip));

// Construct embed
const embed = new EmbedBuilder()
  .setTitle("System Power ON!")
  .setDescription("Download attached decrypt.txt!")
  .setColor("Green");

// Write file
writeFile("./decrypt.txt", encrypt, (err) => {
  if (err) {
    console.error(err);
  }
});

// Construct attachment
const attachment = new AttachmentBuilder("./decrypt.txt");

// Send to file and text to Discord Webhook
webhookClient.send({
  username: "Discord IP Logger",
  avatarURL:
    "https://gitlab.com/ubinull/discord-ip-logger/-/raw/e0a4bf3116f5df86b3d33f5bed7ee67b05b54ece/assets/avatar.png",
  embeds: [embed],
  files: [attachment],
});
