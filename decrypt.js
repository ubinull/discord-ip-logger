// This script can be used to decrypt the recived encrypted IP addresses.

// Use: download the decrypt.txt got sent to Discord by the Webhook,
// place it to the project root, then run 'node decrypt.js'
import { readFileSync } from "fs";
import { privateDecrypt } from "crypto";
import * as dotenv from "dotenv";
dotenv.config();

const path = "./decrypt.txt";

try {
  const data = readFileSync(path);
  console.log("File content:", data);
  const decrypt = privateDecrypt(process.env.PRIVATE_KEY, data);
  console.log(decrypt.toString("utf-8"));
} catch (err) {
  console.error(err);
}
